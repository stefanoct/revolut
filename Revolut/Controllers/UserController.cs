﻿
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Revolut.Services;

namespace Revolut.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet("{id}")]
        public IActionResult GetUser(int id)
        {
            var user = _userService.GetUser(id);

            if (user == null)
                return NotFound($"The user with id = {id} does not exist ");

            return Ok(user);
        }

        [HttpGet("search")]
        public IActionResult GetUserByName([FromQuery]string firstName, [FromQuery]string lastName)
        {
            var users = _userService.GetUserByName(firstName, lastName);

            if (users.Count == 0)
                return NotFound($"The users are not found ");

            return Ok(users);
        }

        [HttpGet()]
        public JsonResult GetUsers()
        {
            var users = _userService.GetUsers();

            return new JsonResult(users);
        }


    }
}