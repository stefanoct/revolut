﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Revolut.Model;

namespace Revolut.Services
{
    public interface IUserService
    {
        IList<User> GetUsers();

        User GetUser(int id);

        IList<User> GetUserByName(string firstName, string lastName);
    }
}
