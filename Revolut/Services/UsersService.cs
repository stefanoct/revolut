﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Revolut.Model;

namespace Revolut.Services
{
    public class UsersService : IUserService
    {
        public IList<User> users = new List<User>
        {
            new User { Id = 1, FirstName = "Andrei", LastName = "Pop"},
            new User { Id = 2, FirstName = "Tudor", LastName = "Toader"},
            new User { Id = 3, FirstName = "Mihai", LastName = "Popa"},
            new User { Id = 4, FirstName = "Ion", LastName = "Pop"},
            new User { Id = 5, FirstName = "Alex", LastName = "Rotari"}
        };

        public User GetUser(int id)
        {
            return users.Where(u => u.Id == id).FirstOrDefault();
        }

        public IList<User> GetUserByName(string firstName, string lastName)
        {
            return users.Where(u => u.FirstName == firstName || u.LastName == lastName).ToList();
        }

        public IList<User> GetUsers()
        {
            return users;
        }

    }
}
